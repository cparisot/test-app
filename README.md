# test-app

simple test application

## build
```
cd app
docker build test-app:test .
```

##
Run
```
docker run -p 8080:8080 test-app:test
```

[hit with this link](http://localhost:8080)

[check metric ](http://localhost:8080/metrics)
